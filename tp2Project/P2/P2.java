package P2;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.Connection;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.ssl.HttpsURLConnection;

public class P2 {
    static String subscriptionKey = "9791f38072e74777a4fbd3cfd195e046";

    static String host = "https://api.microsofttranslator.com";
    static String path = "/V2/Http.svc/Translate";

    static String target = "fr-fr";
	public static void main(String[] argv) throws Exception {

		String EXCHANGE_NAME_TEXT = "text";
                String EXCHANGE_NAME_BD = "bd-text";
		String nomUtilisateur = "p1p2"; // par defaut
		String motDePasse = "p1p2"; // par defaut
		int numeroPort = 5672; // par defaut
		String virtualHostName = "/"; // par defaut
		String hostName = "p1p2.local";
		boolean autoAck = false;
		boolean durable = true; 

		// se connecter au broker RabbitMQ
		ConnectionFactory factory = new ConnectionFactory();

		// indiquer les parametres de la connexion
		factory.setUsername(nomUtilisateur);
		factory.setPassword(motDePasse);
		factory.setPort(numeroPort);
		factory.setVirtualHost(virtualHostName);
		factory.setHost(hostName);

		// autre alternative pour specifier les parametres de la connexion
		// factory.setUri("amqp://nomUtilisateur:motDePasse@hostName:numeroPort/virtualHostName");

		// creer une nouvelle connexion
		Connection connexion = factory.newConnection();

		// ouvrir un canal de communication avec le Broker pour l'envoi et la
		// reception de messages
		Channel canalDeCommunication = connexion.createChannel();

		// declarer une file d'attente nommee NOM_FILE_DATTENTE
		canalDeCommunication.exchangeDeclare(EXCHANGE_NAME_TEXT, "topic");
		String queueName = canalDeCommunication.queueDeclare().getQueue();
                canalDeCommunication.queueBind(EXCHANGE_NAME_TEXT, EXCHANGE_NAME_TEXT, EXCHANGE_NAME_TEXT);
                
		System.out.println(" -* En attente de messages ... pour arreter pressez CTRL+C");

		Consumer consumer = new DefaultConsumer(canalDeCommunication) {
		      @Override
		      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
		          throws IOException {
		        String message = new String(body, "UTF-8");
		        System.out.println(" - Message recu: '" + message + "'");
                        String reponse;
                        try{
                            reponse = Translate (message);
                            System.out.println(" - Message traduit: '" + reponse + "'");
                            String send;
                            send = message + '|' + reponse;
                            Channel canalDeCommunicationText = connexion.createChannel();
                            
                            canalDeCommunicationText.exchangeDeclare(EXCHANGE_NAME_BD, "topic");
                            canalDeCommunicationText.basicPublish(EXCHANGE_NAME_BD, EXCHANGE_NAME_BD, null, send.getBytes());
                            
                        }
                        catch (Exception e) {
                            System.out.println (e);
                        }
		      }
		    };
		canalDeCommunication.basicConsume(EXCHANGE_NAME_TEXT, true, consumer);		
	}
        
    public static String Translate (String text) throws Exception {
        String encoded_query = URLEncoder.encode (text, "UTF-8");
        String params = "?to=" + target + "&text=" + text;
        String resultParse;
        int indexDebut;
        int indexFin;
        URL url = new URL (host + path + params);

        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Ocp-Apim-Subscription-Key", subscriptionKey);
        connection.setDoOutput(true);

        StringBuilder response = new StringBuilder ();
        BufferedReader in = new BufferedReader(
        new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            response.append(line);
        }
        in.close();
        indexDebut = response.indexOf(">") + 1;
        indexFin = response.lastIndexOf("<");
        resultParse = response.substring(indexDebut, indexFin);
        return resultParse;
    }
}


