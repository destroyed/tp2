package P1;

import java.util.concurrent.TimeoutException;

import java.io.IOException;

import com.rabbitmq.client.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
                
import java.nio.file.Files;
import java.io.File;

public class P1 {

    public static void main(String[] argv) throws java.io.IOException {

        String EXCHANGE_NAME_TEXT = "text";
        String EXCHANGE_NAME_IMAGE = "image";
        String NOM_FICHIER = "fanout/simple/data.json";
        String nomUtilisateur = "p1p2"; // par defaut
        String motDePasse = "p1p2"; // par defaut
        int numeroPort = 5672;  // par defaut
        String virtualHostName = "/"; // par defaut
        String hostName = "p1p2.local";
        //String hostName = "192.168.183.129";
        boolean durable = true;

        // se connecter au broker RabbitMQ
        ConnectionFactory factory = new ConnectionFactory();

        // indiquer les parametres de la connexion
        factory.setUsername(nomUtilisateur);
        factory.setPassword(motDePasse);
        factory.setPort(numeroPort);
        factory.setVirtualHost(virtualHostName);
        factory.setHost(hostName);

        String[] texts = new String[5];
        texts[0] = "girls";
        texts[1] = "never";
        texts[2] = "goodbye";
        texts[3] = "molecule";
        texts[4] = "cold";
        
        String[] imagesPath = new String[5];
        imagesPath[0] = "P1/images/image1.jpg";
        imagesPath[1] = "P1/images/image2.jpg";
        imagesPath[2] = "P1/images/image3.jpg";
        imagesPath[3] = "P1/images/image4.jpg";
        imagesPath[4] = "P1/images/image5.jpg";
        
        Connection connexion;
        
        try {
            connexion = factory.newConnection();

            Channel canalDeCommunicationText = connexion.createChannel();
            canalDeCommunicationText.exchangeDeclare(EXCHANGE_NAME_TEXT, "topic");
            
            Channel canalDeCommunicationImage = connexion.createChannel();
            canalDeCommunicationImage.exchangeDeclare(EXCHANGE_NAME_IMAGE, "topic");
            /*Channel canalDeCommunicationImage = connexion.createChannel();
            canalDeCommunicationImage.queueDeclare(NOM_FILE_DATTENTE_IMAGE, durable, false, false, null);
            canalDeCommunicationImage.basicQos(1);*/
            
            for (String text : texts){
                canalDeCommunicationText.basicPublish(EXCHANGE_NAME_TEXT, EXCHANGE_NAME_TEXT, null, text.getBytes());
            }

            for (String text : imagesPath){
                File fi = new File(text);
                byte[] fileContent = Files.readAllBytes(fi.toPath());
                canalDeCommunicationImage.basicPublish(EXCHANGE_NAME_IMAGE, EXCHANGE_NAME_IMAGE, null, fileContent);
            }
            canalDeCommunicationText.close();
            canalDeCommunicationImage.close();

            
            connexion.close();
            
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}


