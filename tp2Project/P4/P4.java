package P4;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import java.awt.image.BufferedImage;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class P4 {
                 
    private static BufferedImage imgBuff1 = null;
    private static BufferedImage imgBuff2 = null;
    private static BufferedImage imgBuff3 = null;
    private static String imgOriginal = null;         
    private static java.sql.Connection conn = null;
	public static void main(String[] argv) throws Exception {

		String EXCHANGE_NAME_TEXT = "bd-text";
                String EXCHANGE_NAME_IMAGE = "bd-image";
		String nomUtilisateur = "p1p2"; // par defaut
		String motDePasse = "p1p2"; // par defaut
		int numeroPort = 5672; // par defaut
		String virtualHostName = "/"; // par defaut
		String hostName = "p1p2.local";
		boolean autoAck = false;
		boolean durable = true; 

		// se connecter au broker RabbitMQ
		ConnectionFactory factory = new ConnectionFactory();

		// indiquer les parametres de la connexion
		factory.setUsername(nomUtilisateur);
		factory.setPassword(motDePasse);
		factory.setPort(numeroPort);
		factory.setVirtualHost(virtualHostName);
		factory.setHost(hostName);

		// autre alternative pour specifier les parametres de la connexion
		// factory.setUri("amqp://nomUtilisateur:motDePasse@hostName:numeroPort/virtualHostName");

		// creer une nouvelle connexion
		com.rabbitmq.client.Connection connexion = factory.newConnection();

		// ouvrir un canal de communication avec le Broker pour l'envoi et la
		// reception de messages
		Channel canalDeCommunicationText = connexion.createChannel();
                
                Channel canalDeCommunicationImage = connexion.createChannel();
		// declarer une file d'attente nommee NOM_FILE_DATTENTE
		canalDeCommunicationText.exchangeDeclare(EXCHANGE_NAME_TEXT, "topic");
		String queueNameText = canalDeCommunicationText.queueDeclare().getQueue();
                canalDeCommunicationText.queueBind(EXCHANGE_NAME_TEXT, EXCHANGE_NAME_TEXT, EXCHANGE_NAME_TEXT);
                
		canalDeCommunicationImage.exchangeDeclare(EXCHANGE_NAME_IMAGE, "topic");
		String queueNameImage = canalDeCommunicationImage.queueDeclare().getQueue();
                canalDeCommunicationImage.queueBind(EXCHANGE_NAME_IMAGE, EXCHANGE_NAME_IMAGE, EXCHANGE_NAME_IMAGE);
                
                
                try {
                    conn =
                       DriverManager.getConnection("jdbc:mysql://localhost/tp2?" +
                                                   "user=tp2&password=!tp2Abcdefgh");
                } catch (SQLException ex) {
                    // handle any errors
                    System.out.println("SQLException: " + ex.getMessage());
                    System.out.println("SQLState: " + ex.getSQLState());
                    System.out.println("VendorError: " + ex.getErrorCode());
                }
                
		System.out.println(" -* En attente de messages ... pour arreter pressez CTRL+C");

		Consumer consumerText = new DefaultConsumer(canalDeCommunicationText) {
		      @Override
		      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
		          throws IOException {
                        String message = new String(body, "UTF-8");
		        System.out.println(" - Message recu: '" + message + "'");
                        int poss = message.indexOf("|");
                        String en = message.substring(0,poss);
                        String fr = message.substring(poss, message.length()-1);
                        Statement stmt = null;
                        try {
                            stmt = conn.createStatement();
                            String sql = "INSERT INTO text (fr,en) values (\'"+fr + "\',\'"+en +"\');";
                            stmt.executeUpdate(sql);
                        }
                        catch (SQLException ex){
                            // handle any errors
                            System.out.println("SQLException: " + ex.getMessage());
                            System.out.println("SQLState: " + ex.getSQLState());
                            System.out.println("VendorError: " + ex.getErrorCode());
                        }
                        finally {
                            if (stmt != null) {
                                try {
                                    stmt.close();
                                } catch (SQLException sqlEx) { } // ignore

                                stmt = null;
                            }
                        }
                        
		      }
		    };
		canalDeCommunicationText.basicConsume(EXCHANGE_NAME_TEXT, true, consumerText);	
                
                Consumer consumerImage = new DefaultConsumer(canalDeCommunicationImage) {
		      @Override
		      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
		          throws IOException {
		        System.out.println(" - image recu ");
                        try{ 
                            Statement stmt = null;
                            try {
                                stmt = conn.createStatement();
                                String sql = "INSERT INTO image (image) values (\'"+ body +"\');";
                                stmt.executeUpdate(sql);
                            }
                            catch (SQLException ex){
                                // handle any errors
                                System.out.println("SQLException: " + ex.getMessage());
                                System.out.println("SQLState: " + ex.getSQLState());
                                System.out.println("VendorError: " + ex.getErrorCode());
                            }
                            finally {
                                if (stmt != null) {
                                    try {
                                        stmt.close();
                                    } catch (SQLException sqlEx) { } // ignore

                                    stmt = null;
                                }
                            }
                            
                        }
                        catch (Exception e) {
                            System.out.println (e);
                        }
		      }
		    };
		canalDeCommunicationImage.basicConsume(EXCHANGE_NAME_IMAGE, true, consumerImage);
	}
}


