package P3;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.Connection;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import java.awt.image.BufferedImage;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import sun.misc.BASE64Decoder;
import net.coobird.thumbnailator.*;
import sun.misc.BASE64Encoder;

public class P3 {
                 
    private static BufferedImage imgBuff1 = null;
    private static BufferedImage imgBuff2 = null;
    private static BufferedImage imgBuff3 = null;
    private static String imgOriginal = null;         

	public static void main(String[] argv) throws Exception {

		String EXCHANGE_NAME_TEXT = "image";
                String EXCHANGE_NAME_BD = "bd-image";
		String nomUtilisateur = "p1p2"; // par defaut
		String motDePasse = "p1p2"; // par defaut
		int numeroPort = 5672; // par defaut
		String virtualHostName = "/"; // par defaut
		String hostName = "p1p2.local";
		boolean autoAck = false;
		boolean durable = true; 

		// se connecter au broker RabbitMQ
		ConnectionFactory factory = new ConnectionFactory();

		// indiquer les parametres de la connexion
		factory.setUsername(nomUtilisateur);
		factory.setPassword(motDePasse);
		factory.setPort(numeroPort);
		factory.setVirtualHost(virtualHostName);
		factory.setHost(hostName);

		// autre alternative pour specifier les parametres de la connexion
		// factory.setUri("amqp://nomUtilisateur:motDePasse@hostName:numeroPort/virtualHostName");

		// creer une nouvelle connexion
		Connection connexion = factory.newConnection();

		// ouvrir un canal de communication avec le Broker pour l'envoi et la
		// reception de messages
		Channel canalDeCommunication = connexion.createChannel();

		// declarer une file d'attente nommee NOM_FILE_DATTENTE
		canalDeCommunication.exchangeDeclare(EXCHANGE_NAME_TEXT, "topic");
		String queueName = canalDeCommunication.queueDeclare().getQueue();
                canalDeCommunication.queueBind(EXCHANGE_NAME_TEXT, EXCHANGE_NAME_TEXT, EXCHANGE_NAME_TEXT);
                
		
		System.out.println(" -* En attente de messages ... pour arreter pressez CTRL+C");

		Consumer consumer = new DefaultConsumer(canalDeCommunication) {
		      @Override
		      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
		          throws IOException {
                        BufferedImage imgBuff1 = ImageIO.read(new ByteArrayInputStream(body));
		        System.out.println(" - image recu ");
                        byte[] imageByte1;
                        String img100 = null;
                        String img400 = null;
                        try{ 
                            imgBuff2 = Thumbnails.of(imgBuff1).size(100, 100).asBufferedImage();
                            
                            imgBuff3 = Thumbnails.of(imgBuff1).size(400, 300).asBufferedImage();
                            
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            ImageIO.write(imgBuff2, "JPG", bos);
                            byte[] imageBytes1 = bos.toByteArray();
                            bos.close();
                            
                            bos = new ByteArrayOutputStream();
                            ImageIO.write(imgBuff3, "JPG", bos);
                            byte[] imageBytes2 = bos.toByteArray();
                            bos.close();
                            
                            Channel canalDeCommunicationText = connexion.createChannel();
                            
                            canalDeCommunicationText.exchangeDeclare(EXCHANGE_NAME_BD, "topic");
                            canalDeCommunicationText.basicPublish(EXCHANGE_NAME_BD, EXCHANGE_NAME_BD, null, body);
                            canalDeCommunicationText.basicPublish(EXCHANGE_NAME_BD, EXCHANGE_NAME_BD, null, imageBytes1);
                            canalDeCommunicationText.basicPublish(EXCHANGE_NAME_BD, EXCHANGE_NAME_BD, null, imageBytes2);
                            
                            
                        }
                        catch (Exception e) {
                            System.out.println (e);
                        }
		      }
		    };
		canalDeCommunication.basicConsume(EXCHANGE_NAME_TEXT, true, consumer);		
	}
}

